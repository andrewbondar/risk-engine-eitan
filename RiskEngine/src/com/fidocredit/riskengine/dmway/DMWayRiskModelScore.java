package com.fidocredit.riskengine.dmway;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.dmway.scoring.neu_alc_onp_001;
import com.dmway.scoring.score.AbstractScorer;
import com.fidocredit.riskengine.DataEnrichment;
import com.fidocredit.riskengine.RiskInputParams;
import com.fidocredit.riskengine.RiskModelScore;
import com.fidocredit.riskengine.RiskModelScoreResult;
import com.fidocredit.riskengine.exceptions.InputParseException;
import com.fidocredit.riskengine.exceptions.RiskEngineApplicativeException;
import com.fidocredit.riskengine.lms.Account;

public class DMWayRiskModelScore implements RiskModelScore {

	private DataEnrichment de;	
	
	public DMWayRiskModelScore(DataEnrichment de) {
		// TODO replace this with CDI & qualifiers at the factory level
		this.de = de;
	}
	
	@Override
	public RiskModelScoreResult scoreModel(Map<String, Object> params) {
		Logger.getLogger(this.getClass()).debug("calling neu_alc_onp_001.score for loan id " + params.get("ticket_id"));
		// TODO replace the specific model creation with DB level configuration for mapping qualifiers to risk models
		AbstractScorer riskModel = new neu_alc_onp_001();
		com.dmway.scoring.score.ScoreResult result = riskModel.score(params);
		DMWayScoreResult dmResult = new DMWayScoreResult(riskModel, result);
		// TODO make the threashold a configurable parameter through DB settings
		dmResult.setAccepted(result.getScore() < 0.7);
		return dmResult;
	}

	@Override
	public Map<String, Object> enrichData(Map<String, Object> params) throws RiskEngineApplicativeException {
		Logger.getLogger(this.getClass()).debug("enrichData for loan id " + params.get(RiskInputParams.LOAN_ID.getParamName()));
		JSONParser parser = new JSONParser();
		JSONObject survey = null;
		
		try {
			survey = (JSONObject) parser.parse(params.get("survey").toString());
		} catch (ParseException e) {
			throw new InputParseException("error parsing survey info");
		}
//			JSONObject mobile_data = (JSONObject) parser.parse(params.get("mobile_data").toString());
//			JSONObject phone_data = (JSONObject) parser.parse(params.get("phone_data").toString());
			
		// TODO needs better formatting technique to format input params into model params
		
		// TODO needs to recalculate the how many references
		Double noOfRef = Double.valueOf(de.getHowManyReferences(params));
		// TODO needs to recalculate the reference score
		Double refScore = Double.valueOf(de.getReferenceScore(params));
		Map<String, Object> newParams = new HashMap<String, Object>();
		newParams.put("howmany_references", noOfRef);
		newParams.put("reference_score", refScore);
		newParams.put("address_nchar", getSurveyParam(survey, "address") != null ? Double.valueOf(getSurveyParam(survey, "address").length()) : 0.0);
		Double loan_term = Double.valueOf(getSurveyParam(survey, "loan_term") != null ? getSurveyParam(survey, "loan_term") : "0");
		Double loan_amount = Double.valueOf(getSurveyParam(survey, "loan_amount") != null ? getSurveyParam(survey, "loan_amount") : "0");
		Double income = Double.valueOf(getSurveyParam(survey, "income") != null ? getSurveyParam(survey, "income") : "1");
		String income_frequency = getSurveyParam(survey, "income_frequency");
		double loan_norm_multiplier = income_frequency == null ? 1 : (income_frequency.equals("Per month") ? 1 : (income_frequency.equals("Per week") ? 4.2 : (income_frequency.equals("Per day") ? 23 : 1)));
		newParams.put("income_norm", income * loan_norm_multiplier);
		newParams.put("loan_to_income", loan_amount/(double)newParams.get("income_norm"));

		try {
			Calendar calendar2 = Calendar.getInstance(); // now
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String dobs = getSurveyParam(survey, "date_of_birth");
			if (dobs != null) { 
				Date dob = sdf.parse(dobs);
				calendar2.setTime(dob);
	
			    Calendar calendar1 = Calendar.getInstance(); // now
	
			    int years = calendar1.get(Calendar.YEAR) - calendar2.get(Calendar.YEAR);
			    int days = calendar1.get(Calendar.DAY_OF_YEAR) - calendar2.get(Calendar.DAY_OF_YEAR);
			    
				newParams.put("age", Double.valueOf(years + days/365));
			} else {
				newParams.put("age", Double.valueOf(0));
			}
		} catch (java.text.ParseException e) {
			throw new InputParseException("date_of_birth expected in format yyyy-mm-dd");
		}
		
		newParams.put("is_kiosk", getSurveyParam(survey, "kiosk_id") != null ? 1.0 : 0.0);

		List<Account> accounts = de.getAccounts(params);
		newParams.put("number_of_accounts", Double.valueOf(accounts.size()));
		boolean has_facebook = false;
		boolean has_viber = false;
		boolean has_skype = false;
		for (int i = 0; i < accounts.size(); i++) {
			if (accounts.get(i).getType().toLowerCase().startsWith("com.facebook")) {
				has_facebook = true;
			} else if (accounts.get(i).getType().toLowerCase().startsWith("com.skype")) {
				has_skype = true;
			} else if (accounts.get(i).getType().toLowerCase().startsWith("com.viber.voip")) {
				has_viber = true;
			}
		}
		newParams.put("has_facebook", has_facebook ? "True" : "False"); // this is the case sensitive the DMWay model expects it
		newParams.put("has_viber", has_viber ? "True" : "False"); // this is the case sensitive the DMWay model expects it
		newParams.put("has_skype", has_skype ? "True" : "False"); // this is the case sensitive the DMWay model expects it

		// unchanged input as they arrive and passed the same way with no changes
		newParams.put("loan_amount", loan_amount);
		newParams.put("loan_term", loan_term);
		newParams.put("city", getSurveyParam(survey, "city"));
		newParams.put("gender", getSurveyParam(survey, "gender"));
		newParams.put("industry", getSurveyParam(survey, "industry"));
		newParams.put("phone_number", getSurveyParam(survey, "phone_number"));
		
		newParams.put("loan_uuid", params.get(RiskInputParams.LOAN_UUID.getParamName()).toString());
		newParams.put("ticket_id", params.get(RiskInputParams.LOAN_ID.getParamName()).toString());
		
		return newParams;

	}
	
	private String getSurveyParam(JSONObject survey, String paramName) {
		return survey.containsKey(paramName) ? (survey.get(paramName) != null ? survey.get(paramName).toString() : null) : null;
	}

}

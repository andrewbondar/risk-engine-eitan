package com.fidocredit.riskengine.dmway;

import java.util.Map;

import com.dmway.scoring.score.AbstractScorer;
import com.fidocredit.riskengine.RiskModelScoreResult;

public class DMWayScoreResult implements RiskModelScoreResult {

	private com.dmway.scoring.score.ScoreResult scoreResult;
	private boolean isAccepted = false;
	private String model = null;
	
	public DMWayScoreResult(AbstractScorer scorer, com.dmway.scoring.score.ScoreResult scoreResult) {
		this.model = scorer.getClass().getSimpleName();
		this.scoreResult = scoreResult;
	}
	
	@Override
	public Double getScore() {
		return scoreResult.getScore();
	}

	@Override
	public Map<String, Number> getCoefficients() {
		return scoreResult.getTransformations();
	}

	@Override
	public boolean isAccepted() {
		return isAccepted;
	}

	@Override
	public void setAccepted(boolean isAccepted) {
		this.isAccepted = isAccepted;
	}

	@Override
	public String getModel() {
		return model;
	}

}

package com.fidocredit.riskengine.dmway;

import java.util.Map;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import com.fidocredit.riskengine.RiskInputParams;
import com.fidocredit.riskengine.RiskModelFactory;
import com.fidocredit.riskengine.RiskModelScore;
import com.fidocredit.riskengine.exceptions.NoCorrespondingRiskModel;
import com.fidocredit.riskengine.exceptions.RiskEngineApplicativeException;
import com.fidocredit.riskengine.lms.DataEnricher;

public class DMWayRiskModelFactory extends RiskModelFactory {

	@Inject DataEnricher de;
	
	public DMWayRiskModelFactory() {

	}
	
	@Override
	public RiskModelScore getRiskModelScore(Map<String, Object> params) throws RiskEngineApplicativeException {		
		// TODO find the proper model to apply according to qualifiers
		Logger.getLogger(this.getClass()).debug("calling DataEnricher.isNewCustomer for loan id " + params.get(RiskInputParams.LOAN_ID.getParamName()));
		if (de.isNewCustomer(params)) {		
			return new DMWayRiskModelScore(de);
		}
		
		throw new NoCorrespondingRiskModel();
	}

}

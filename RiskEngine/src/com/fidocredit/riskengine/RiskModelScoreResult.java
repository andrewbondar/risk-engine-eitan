package com.fidocredit.riskengine;

import java.util.Map;

public interface RiskModelScoreResult {

	public Double getScore();

	public Map<String, Number> getCoefficients();
	
	public boolean isAccepted();
	
	public void setAccepted(boolean isAccepted);

	public String getModel();

}

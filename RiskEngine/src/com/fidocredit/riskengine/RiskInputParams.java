package com.fidocredit.riskengine;

public enum RiskInputParams {

	LOAN_ID("loan_id"), 
	LOAN_UUID("loan_uuid"), 
	SURVEY("survey"), 
	MOBILE_DATA("mobile_data");
	
	private RiskInputParams(String paramName) {
		this.paramName = paramName;
	}
	
	private String paramName;

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	
}

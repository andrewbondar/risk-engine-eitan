package com.fidocredit.riskengine;

import java.util.Map;

import javax.inject.Inject;

import org.apache.log4j.Logger;

import com.fidocredit.riskengine.dmway.DMWayRiskModelFactory;
import com.fidocredit.riskengine.exceptions.InputParseException;
import com.fidocredit.riskengine.exceptions.RiskEngineApplicativeException;
import com.fidocredit.riskengine.lms.DataEnricher;

public class RiskEngine  {

	@Inject DMWayRiskModelFactory rmf;
	@Inject DataEnricher de;
	
	public RiskEngine() {

	}
	
	public final RiskModelScoreResult modelScoring(Map<String, Object> params) throws RiskEngineApplicativeException {
		if (!params.containsKey(RiskInputParams.LOAN_ID.getParamName()))
			throw new InputParseException("missing loan_id info");
		
		if (!params.containsKey(RiskInputParams.LOAN_UUID.getParamName()))
			params.put(RiskInputParams.LOAN_UUID.getParamName(), de.getLoanUuid(Long.valueOf((String)params.get(RiskInputParams.LOAN_ID.getParamName()))));
		
		if (!params.containsKey(RiskInputParams.SURVEY.getParamName()) || params.get(RiskInputParams.SURVEY.getParamName()) == null)
			params.put(RiskInputParams.SURVEY.getParamName(), de.getSurvey((String)params.get(RiskInputParams.LOAN_UUID.getParamName())).toJSONString());
		
		if (!params.containsKey(RiskInputParams.MOBILE_DATA.getParamName()) || params.get(RiskInputParams.MOBILE_DATA.getParamName()) == null)
			throw new InputParseException("missing mobile_data info");	
		
		Logger.getLogger(this.getClass()).debug("calling DMWayRiskModelFactory.getRiskModelScore for loan id " + params.get(RiskInputParams.LOAN_ID.getParamName()));
		return modelScoring(rmf.getRiskModelScore(params), params);
	}
	
	private final RiskModelScoreResult modelScoring(RiskModelScore rms, Map<String, Object> params) throws RiskEngineApplicativeException {
		Logger.getLogger(this.getClass()).debug("enriching data for loan id " + params.get(RiskInputParams.LOAN_ID.getParamName()));
		Map<String, Object> new_params = rms.enrichData(params);
		Logger.getLogger(this.getClass()).info("injecting params to risk model for loan id " + params.get(RiskInputParams.LOAN_ID.getParamName()) + " : " + new_params.toString());
		return rms.scoreModel(new_params);		
	}

}

package com.fidocredit.riskengine.exceptions;

public class InputParseException extends RiskEngineApplicativeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5923943804335712373L;

	public InputParseException() {
		super();
	}
	
	public InputParseException(String message) {
		super(message);
	}
	
}

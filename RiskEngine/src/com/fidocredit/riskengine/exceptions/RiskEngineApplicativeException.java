package com.fidocredit.riskengine.exceptions;

public abstract class RiskEngineApplicativeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7783566386311808983L;

	public RiskEngineApplicativeException() {
		super();
	}
	
	public RiskEngineApplicativeException(String message) {
		super(message);
	}	
}

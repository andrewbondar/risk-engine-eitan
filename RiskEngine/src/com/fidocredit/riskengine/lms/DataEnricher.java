package com.fidocredit.riskengine.lms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.StringTokenizer;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
//import org.apache.log4j.LogManager;
//import org.apache.log4j.Logger;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.fidocredit.riskengine.DataEnrichment;
import com.fidocredit.riskengine.RiskInputParams;
import com.fidocredit.riskengine.exceptions.InputParseException;
import com.fidocredit.riskengine.exceptions.NoLoanTicketFoundForLoanUuid;
import com.fidocredit.riskengine.exceptions.RiskEngineApplicativeException;
import com.fidocredit.riskengine.lms.entity.Survey;
import com.fidocredit.riskengine.lms.entity.SurveyPK_;
import com.fidocredit.riskengine.lms.entity.Survey_;
import com.fidocredit.riskengine.lms.entity.Ticket;
import com.fidocredit.riskengine.lms.entity.TicketChange;
import com.fidocredit.riskengine.lms.entity.TicketChangePK_;
import com.fidocredit.riskengine.lms.entity.TicketChange_;
import com.fidocredit.riskengine.lms.entity.TicketCustom;
import com.fidocredit.riskengine.lms.entity.TicketCustomPK_;
import com.fidocredit.riskengine.lms.entity.TicketCustom_;
import com.fidocredit.riskengine.lms.entity.Ticket_;

public class DataEnricher implements DataEnrichment {

    @PersistenceContext(unitName = "LMS")
    private EntityManager entityManager;
	
//    private static final Logger logger = LogManager.getLogger(DataEnricher.class);
    
    public DataEnricher() {
		
	}

    /* (non-Javadoc)
	 * @see com.fidocredit.riskengine.lms.DataEnrichment#getReferenceScore(java.util.Map)
	 */
    @Override
	public Double getReferenceScore(Map<String, Object> params) throws RiskEngineApplicativeException {
    	Logger.getLogger(this.getClass()).debug("LMS getReferenceScore for loan id " + params.get(RiskInputParams.LOAN_ID.getParamName()));
    	JSONObject surveyData = getSurvey(params);
    	double reference_score = 0; 

    	CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    	CriteriaQuery<TicketCustom> cqtc = cb.createQuery(TicketCustom.class);
    	Root<TicketCustom> ticketc = cqtc.from(TicketCustom.class);
    	List<Object> phoneNos = Arrays.asList(surveyData.get("reference1_phone_number"), surveyData.get("reference2_phone_number"), surveyData.get("reference3_phone_number"));
    	Predicate phoneNo = cb.and(cb.equal(ticketc.get(TicketCustom_.id).get(TicketCustomPK_.name), "phone_number"), ticketc.get(TicketCustom_.value).in(phoneNos));
    	cqtc.where(phoneNo);
    	cqtc.select(ticketc);
    	TypedQuery<TicketCustom> q = entityManager.createQuery(cqtc); 
    	List<Integer> ticketIds = new ArrayList<Integer>();
    	for (int i = 0; i < q.getResultList().size(); i++) {
    		ticketIds.add(q.getResultList().get(i).getId().getTicket());
		}

    	cb = entityManager.getCriteriaBuilder();
    	CriteriaQuery<Survey> cqs = cb.createQuery(Survey.class);
    	Root<Survey> survey = cqs.from(Survey.class);
    	Predicate ref1 = cb.and(cb.equal(survey.get(Survey_.id).get(SurveyPK_.fieldName), "reference1_phone_number"), cb.equal(survey.get(Survey_.fieldValue), surveyData.get("phone_number")));
    	Predicate ref2 = cb.and(cb.equal(survey.get(Survey_.id).get(SurveyPK_.fieldName), "reference2_phone_number"), cb.equal(survey.get(Survey_.fieldValue), surveyData.get("phone_number")));
    	Predicate ref3 = cb.and(cb.equal(survey.get(Survey_.id).get(SurveyPK_.fieldName), "reference3_phone_number"), cb.equal(survey.get(Survey_.fieldValue), surveyData.get("phone_number")));
    	cqs.where(cb.or(cb.or(ref1, ref2), ref3));
    	cqs.select(survey);
    	TypedQuery<Survey> tqs = entityManager.createQuery(cqs);

    	List<String> uuids = new ArrayList<String>();
    	for (int i = 0; i < tqs.getResultList().size(); i++) {
    		String uuid = tqs.getResultList().get(i).getId().getUuid();
    		if (uuids.indexOf(uuid) == -1) // prevent duplications
    			uuids.add(uuid);
		}    	
    	
    	if (uuids.size() > 0) {
	    	cb = entityManager.getCriteriaBuilder();
	    	CriteriaQuery<TicketCustom> cq = cb.createQuery(TicketCustom.class);
	    	Root<TicketCustom> ticketCustom = cq.from(TicketCustom.class);
	    	Predicate loan_uuid = cb.and(cb.equal(ticketCustom.get(TicketCustom_.id).get(TicketCustomPK_.name), "loan_uuid"), ticketCustom.get(TicketCustom_.value).in(uuids));
	    	cq.where(loan_uuid);
	    	cq.select(ticketCustom);
	    	TypedQuery<TicketCustom> tqtc = entityManager.createQuery(cq);
	    	for (int i = 0; i < tqtc.getResultList().size(); i++) {
	    		Integer ticketId = tqtc.getResultList().get(i).getId().getTicket();
	    		if (ticketIds.indexOf(ticketId) == -1) // prevent duplications
	    			ticketIds.add(ticketId);
			}
    	}
    	
    	if (ticketIds.size() > 0) {
        	cb = entityManager.getCriteriaBuilder();
        	CriteriaQuery<Ticket> cqt = cb.createQuery(Ticket.class);
        	Root<Ticket> ticket = cqt.from(Ticket.class);

        	Object[] closed = {"loan_closed"};
        	Predicate wt = cb.and(ticket.get(Ticket_.id).in(ticketIds), ticket.get(Ticket_.status).in(closed));
        	cqt.where(cb.and(wt, cb.equal(ticket.get(Ticket_.type), "loan")));
        	cqt.select(ticket);
        	TypedQuery<Ticket> tqt = entityManager.createQuery(cqt); 

        	List<Integer> closedTicketIds = new ArrayList<Integer>();
        	for (int i = 0; i < tqt.getResultList().size(); i++) {
        		closedTicketIds.add(tqt.getResultList().get(i).getId());
    		}

        	if (closedTicketIds.size() > 0) {
            	cb = entityManager.getCriteriaBuilder();
            	CriteriaQuery<TicketChange> cqtch = cb.createQuery(TicketChange.class);
            	Root<TicketChange> ticketch = cqtch.from(TicketChange.class);

            	Predicate statuses = cb.and(ticketch.get(TicketChange_.id).get(TicketChangePK_.ticket).in(closedTicketIds), cb.equal(ticketch.get(TicketChange_.id).get(TicketChangePK_.field), "status"));
            	Predicate repaid = cb.and(ticketch.get(TicketChange_.newvalue).in("in_default", "repaid_late"));
            	cqtch.where(cb.and(statuses, repaid));
            	cqtch.select(ticketch);
            	TypedQuery<TicketChange> tqtch = entityManager.createQuery(cqtch);  

            	for (int i = 0; i < tqtch.getResultList().size(); i++) {
            		if (tqtch.getResultList().get(i).getNewvalue().equals("in_default")) {
            			reference_score = reference_score + 3;
            		} else if (tqtch.getResultList().get(i).getNewvalue().equals("repaid_late")) {
            			reference_score = reference_score + 1;
            		}
        		}
        	}
    	}
    	
    	return Double.valueOf(reference_score);
    }
    
    /* (non-Javadoc)
	 * @see com.fidocredit.riskengine.lms.DataEnrichment#getHowManyReferences(java.util.Map)
	 */
    @Override
	public Long getHowManyReferences(Map<String, Object> params) throws RiskEngineApplicativeException {
    	Logger.getLogger(this.getClass()).debug("LMS getHowManyReferences for loan id " + params.get(RiskInputParams.LOAN_ID.getParamName()));
    	JSONObject surveyData = getSurvey(params);
    	
    	CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    	CriteriaQuery<Long> cq = cb.createQuery(Long.class);
    	Root<Survey> survey = cq.from(Survey.class);
    	
    	List<Object> phoneNos = Arrays.asList(surveyData.get("reference1_phone_number"), surveyData.get("reference2_phone_number"), surveyData.get("reference3_phone_number"));
//    	logger.info("phoneNos : " + phoneNos.toString());
    	Predicate phoneNo = cb.and(cb.equal(survey.get(Survey_.id).get(SurveyPK_.fieldName), "phone_number"), survey.get(Survey_.fieldValue).in(phoneNos));
    	cq.where(phoneNo);
    	cq.select(cb.countDistinct(survey));
    	TypedQuery<Long> q = entityManager.createQuery(cq);
    	
    	return q.getSingleResult();
    }
    
    /* (non-Javadoc)
	 * @see com.fidocredit.riskengine.lms.DataEnrichment#isNewCustomer(java.util.Map)
	 */
    @Override
	public boolean isNewCustomer(Map<String, Object> params) throws RiskEngineApplicativeException {
    	Logger.getLogger(this.getClass()).debug("LMS isNewCustomer for loan id " + params.get(RiskInputParams.LOAN_ID.getParamName()));
    	try {

			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	    	CriteriaQuery<TicketCustom> cqtc = cb.createQuery(TicketCustom.class);
	    	Root<TicketCustom> rtc = cqtc.from(TicketCustom.class);
	    	Predicate mambu_client_id = cb.and(cb.equal(rtc.get(TicketCustom_.id).get(TicketCustomPK_.name), "mambu_client_id"), cb.equal(rtc.get(TicketCustom_.id).get(TicketCustomPK_.ticket), params.get(RiskInputParams.LOAN_ID.getParamName())));
	    	cqtc.where(mambu_client_id);
	    	cqtc.select(rtc);
	    	TypedQuery<TicketCustom> tql = entityManager.createQuery(cqtc);
	    	
			cb = entityManager.getCriteriaBuilder();
	    	cqtc = cb.createQuery(TicketCustom.class);
	    	rtc = cqtc.from(TicketCustom.class);
	    	mambu_client_id = cb.and(cb.equal(rtc.get(TicketCustom_.id).get(TicketCustomPK_.name), "mambu_client_id"), cb.equal(rtc.get(TicketCustom_.value), tql.getSingleResult().getValue()));
	    	cqtc.where(mambu_client_id);
	    	cqtc.select(rtc);
	    	tql = entityManager.createQuery(cqtc);   	
	    	
	    	List<TicketCustom> loans = tql.getResultList();
	    	List<Integer> ticketIds = new ArrayList<Integer>();
	    	for (int i = 0; i < loans.size(); i++) {
				if (loans.get(i).getId().getTicket() != params.get(RiskInputParams.LOAN_ID.getParamName())) // found another ticket for the same mambu_client_id
					ticketIds.add(loans.get(i).getId().getTicket());
			}
	    	
        	cb = entityManager.getCriteriaBuilder();
        	CriteriaQuery<Ticket> cqt = cb.createQuery(Ticket.class);
        	Root<Ticket> ticket = cqt.from(Ticket.class);

        	
        	Predicate tp = cb.and(ticket.get(Ticket_.id).in(ticketIds), cb.equal(ticket.get(Ticket_.type), "loan"));
        	Object[] statuses = {"loan_outstanding", "loan_due", "loan_completed", "loan_overdue"};
        	Predicate sp = ticket.get(Ticket_.status).in(statuses);
        	Object[] resolutions = {"repaid", "repaid_late", "in_default", "reimbursed"};
        	Predicate scp = cb.and(cb.equal(ticket.get(Ticket_.status), "loan_closed"), ticket.get(Ticket_.resolution).in(resolutions));
        	cqt.where(cb.and(tp, cb.or(sp, scp)));
        	cqt.select(ticket);
        	TypedQuery<Ticket> tqt = entityManager.createQuery(cqt); 

        	if (tqt.getResultList().size() > 0) {
        		return false;
        	}

	    	return true;
    	} catch (NoResultException e) {
    		return true;
    	} catch (NonUniqueResultException e) {
    		Logger.getLogger(this.getClass()).error("loan seems to have multiple mambu client ids which shouldnt happen");
    		return false;
    	}
    }
    
    public String getLoanUuid(long ticket_id) throws RiskEngineApplicativeException {
    	Logger.getLogger(this.getClass()).debug("LMS getLoanUuid for ticket id " + ticket_id);
    	CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    	CriteriaQuery<TicketCustom> cq = cb.createQuery(TicketCustom.class);
    	Root<TicketCustom> ticketCustom = cq.from(TicketCustom.class);
    	Predicate loan_uuid = cb.and(cb.equal(ticketCustom.get(TicketCustom_.id).get(TicketCustomPK_.name), "loan_uuid"), cb.equal(ticketCustom.get(TicketCustom_.id).get(TicketCustomPK_.ticket), ticket_id));
    	cq.where(loan_uuid);
    	cq.select(ticketCustom);
    	TypedQuery<TicketCustom> tqtc = entityManager.createQuery(cq);
    	if (tqtc.getResultList().size() == 1) {
    		return tqtc.getResultList().get(0).getValue();
		} else {
			throw new NoLoanTicketFoundForLoanUuid();
		}
    }
    
    public JSONObject getSurvey(String loan_uuid) throws RiskEngineApplicativeException {
    	Logger.getLogger(this.getClass()).debug("LMS getSurvey for loan id " + loan_uuid);
    	CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    	CriteriaQuery<Survey> cqs = cb.createQuery(Survey.class);
    	Root<Survey> survey = cqs.from(Survey.class);
    	Predicate uuid = cb.equal(survey.get(Survey_.id).get(SurveyPK_.uuid), loan_uuid);
    	cqs.where(uuid);
    	cqs.select(survey);
    	TypedQuery<Survey> tqs = entityManager.createQuery(cqs);
    	JSONObject surveyj = new JSONObject();
    	ListIterator<Survey> lis = tqs.getResultList().listIterator();
    	while (lis.hasNext()) {
    		Survey surv = lis.next();
    		String field_name = surv.getId().getFieldName();
    		String field_value = surv.getFieldValue();
//    		System.out.println(i + " : " + field_name + "=" + field_value);
    		surveyj.put(field_name, field_value);
		} 
    	return surveyj;
    }    
    
    private JSONObject getSurvey(Map<String, Object> params) throws RiskEngineApplicativeException {
		JSONParser parser = new JSONParser();
		JSONObject survey = null;
		try {
			survey = (JSONObject) parser.parse(params.get(RiskInputParams.SURVEY.getParamName()).toString());
			return survey;
		} catch (ParseException e) {
			throw new InputParseException("error parsing survey info");
		}    	
    }
    
    /* (non-Javadoc)
	 * @see com.fidocredit.riskengine.lms.DataEnrichment#getAccounts(java.util.Map)
	 */
    @Override
	public List<Account> getAccounts(Map<String, Object> params) throws InputParseException {
    	Logger.getLogger(this.getClass()).debug("LMS getAccounts for loan id " + params.get(RiskInputParams.LOAN_ID.getParamName()));
    	List<Account> accounts = new ArrayList<Account>();
    	Object md = params.get(RiskInputParams.MOBILE_DATA.getParamName());
    	if (md != null) {
//    		Logger.getLogger(this.getClass()).debug("got mobile_data info");
	    	String mobile_data = md.toString();
	    	StringTokenizer str = new StringTokenizer(mobile_data, "\n");
	    	while(str.hasMoreTokens()) {
	    		String record = str.nextToken();
//	    		Logger.getLogger(this.getClass()).debug("inspecting record : " + record);
	    		StringTokenizer stf = new StringTokenizer(record, ",");
	    		if (stf.hasMoreTokens()) {
	    			String rt = stf.nextToken();
//	    			Logger.getLogger(this.getClass()).debug("inspecting field : " + rt);
	    			if (rt.replace("\"", "").equals("0")) { // indicates phone_data
//	    				Logger.getLogger(this.getClass()).debug("found record type 0");
	    				String rst = stf.nextToken();
//	    				Logger.getLogger(this.getClass()).debug("inspecting field : " + rst);
	    				if (rst.replace("\"", "").equals("16")) {
//	    					Logger.getLogger(this.getClass()).debug("found sub record type 16");
	    					Account acc = new Account();
	    					acc.setName(stf.nextToken().replace("\"", ""));
	    					acc.setType(stf.nextToken().replace("\"", ""));
//	    					Logger.getLogger(this.getClass()).debug("found account : " + acc.getName() + " for type " + acc.getType());
	    					accounts.add(acc);
	    				}
	    			}
	    		}
	    	}
    	}
    	
    	return accounts;
    }
    
}

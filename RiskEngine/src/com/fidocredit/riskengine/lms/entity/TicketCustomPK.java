package com.fidocredit.riskengine.lms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the ticket_custom database table.
 * 
 */
@Embeddable
public class TicketCustomPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(unique=true, nullable=false)
	private Integer ticket;

	@Column(unique=true, nullable=false, length=2147483647)
	private String name;

	public TicketCustomPK() {
	}
	public Integer getTicket() {
		return this.ticket;
	}
	public void setTicket(Integer ticket) {
		this.ticket = ticket;
	}
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TicketCustomPK)) {
			return false;
		}
		TicketCustomPK castOther = (TicketCustomPK)other;
		return 
			this.ticket.equals(castOther.ticket)
			&& this.name.equals(castOther.name);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.ticket.hashCode();
		hash = hash * prime + this.name.hashCode();
		
		return hash;
	}
}
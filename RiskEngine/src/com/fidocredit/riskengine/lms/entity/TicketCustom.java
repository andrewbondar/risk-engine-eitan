package com.fidocredit.riskengine.lms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ticket_custom database table.
 * 
 */
@Entity
@Table(name="ticket_custom")
@NamedQuery(name="TicketCustom.findAll", query="SELECT t FROM TicketCustom t")
public class TicketCustom implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TicketCustomPK id;

	@Column(length=2147483647)
	private String value;

	public TicketCustom() {
	}

	public TicketCustomPK getId() {
		return this.id;
	}

	public void setId(TicketCustomPK id) {
		this.id = id;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
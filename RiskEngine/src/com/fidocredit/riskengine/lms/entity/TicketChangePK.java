package com.fidocredit.riskengine.lms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the ticket_change database table.
 * 
 */
@Embeddable
public class TicketChangePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(unique=true, nullable=false)
	private Integer ticket;

	@Column(unique=true, nullable=false)
	private Long time;

	@Column(unique=true, nullable=false, length=2147483647)
	private String field;

	public TicketChangePK() {
	}
	public Integer getTicket() {
		return this.ticket;
	}
	public void setTicket(Integer ticket) {
		this.ticket = ticket;
	}
	public Long getTime() {
		return this.time;
	}
	public void setTime(Long time) {
		this.time = time;
	}
	public String getField() {
		return this.field;
	}
	public void setField(String field) {
		this.field = field;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof TicketChangePK)) {
			return false;
		}
		TicketChangePK castOther = (TicketChangePK)other;
		return 
			this.ticket.equals(castOther.ticket)
			&& this.time.equals(castOther.time)
			&& this.field.equals(castOther.field);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.ticket.hashCode();
		hash = hash * prime + this.time.hashCode();
		hash = hash * prime + this.field.hashCode();
		
		return hash;
	}
}
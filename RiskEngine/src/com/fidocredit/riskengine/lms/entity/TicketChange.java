package com.fidocredit.riskengine.lms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the ticket_change database table.
 * 
 */
@Entity
@Table(name="ticket_change")
@NamedQuery(name="TicketChange.findAll", query="SELECT t FROM TicketChange t")
public class TicketChange implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TicketChangePK id;

	@Column(length=2147483647)
	private String author;

	@Column(length=2147483647)
	private String newvalue;

	@Column(length=2147483647)
	private String oldvalue;

	public TicketChange() {
	}

	public TicketChangePK getId() {
		return this.id;
	}

	public void setId(TicketChangePK id) {
		this.id = id;
	}

	public String getAuthor() {
		return this.author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getNewvalue() {
		return this.newvalue;
	}

	public void setNewvalue(String newvalue) {
		this.newvalue = newvalue;
	}

	public String getOldvalue() {
		return this.oldvalue;
	}

	public void setOldvalue(String oldvalue) {
		this.oldvalue = oldvalue;
	}

}
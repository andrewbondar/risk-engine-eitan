package com.fidocredit.riskengine.lms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the ticket_change database table.
 * 
 */
@Embeddable
public class SurveyPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(unique=true, nullable=false, length=36)
	private String uuid;
	
	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	@Column(unique=true, name="field_name", nullable=false, length=36)
	private String fieldName;

	public SurveyPK() {
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof SurveyPK)) {
			return false;
		}
		SurveyPK castOther = (SurveyPK)other;
		return 
			this.uuid.equals(castOther.uuid)
			&& this.fieldName.equals(castOther.fieldName);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.uuid.hashCode();
		hash = hash * prime + this.fieldName.hashCode();
		
		return hash;
	}
}
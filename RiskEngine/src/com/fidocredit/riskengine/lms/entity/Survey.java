package com.fidocredit.riskengine.lms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the survey database table.
 * 
 */
@Entity
@Table(name="survey")
@NamedQuery(name="Survey.findAll", query="SELECT s FROM Survey s")
public class Survey implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SurveyPK id;
	
	public SurveyPK getId() {
		return id;
	}

	public void setId(SurveyPK id) {
		this.id = id;
	}

	@Column(name="created_on", nullable=false)
	private Timestamp createdOn;

	@Column(name="field_value", nullable=false, length=2147483647)
	private String fieldValue;

	@Column(name="modified_on", nullable=false)
	private Timestamp modifiedOn;

	public Survey() {
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getFieldValue() {
		return this.fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public Timestamp getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

}
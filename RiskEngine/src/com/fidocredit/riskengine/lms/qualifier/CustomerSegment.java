package com.fidocredit.riskengine.lms.qualifier;

import java.util.Map;

import javax.inject.Inject;

import com.fidocredit.riskengine.DataEnrichment;
import com.fidocredit.riskengine.exceptions.RiskEngineApplicativeException;
import com.fidocredit.riskengine.qualifier.Qualifier;

public class CustomerSegment extends com.fidocredit.riskengine.qualifier.CustomerSegment {

	@Inject DataEnrichment de;
	
	@Override
	protected String qualify(Map<String, Object> params) throws RiskEngineApplicativeException {
		Boolean isNewCustomer = de.isNewCustomer(params);
		if (isNewCustomer) {
			return Qualifier.CustomerSegment.NEW.getSegment();
		}
		
		return Qualifier.CustomerSegment.RETURNING.getSegment();
	}


	
}

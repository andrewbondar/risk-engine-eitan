package com.fidocredit.riskengine.qualifier;

import java.util.Map;

import com.fidocredit.riskengine.exceptions.RiskEngineApplicativeException;

public interface Qualifier {

	public enum CustomerSegment {
		ALL("alu"),
		NEW("neu"),
		RETURNING("reu");
		
		private String segment;

		private CustomerSegment(String segment) {
			setSegment(segment);
		}
		
		public String getSegment() {
			return segment;
		}

		public void setSegment(String segment) {
			this.segment = segment;
		}
	}

	public enum ChannelOffering {
		ALL("alu"),
		APPLICATION("apc"),
		WEB("kic"),
		KIOSK("kio");
		
		private ChannelOffering(String channel) {
			setChannel(channel);
		}
		
		private String channel;

		public String getChannel() {
			return channel;
		}

		public void setChannel(String channel) {
			this.channel = channel;
		}

	}
	
	public enum FinancialProduct {
		ALL("alf"),
		ONE_PAYMENT("onp"),
		INSTALLMENTS("inp");
		
		private FinancialProduct(String product) {
			setProduct(product);
		}
		
		private String product;

		public String getProduct() {
			return product;
		}

		public void setProduct(String product) {
			this.product = product;
		}

	}
	
	public boolean isQualified(Map<String, Object> params, String qualifier) throws RiskEngineApplicativeException;
	
}

package com.fidocredit.riskengine.qualifier;

import java.util.Map;
import java.util.StringTokenizer;

import com.fidocredit.riskengine.exceptions.RiskEngineApplicativeException;

public abstract class AbstractQualifier implements Qualifier {

	private int token;
	
	protected AbstractQualifier(int token) {
		setToken(token);
	}
	
	protected int getToken() {
		return token;
	}

	private void setToken(int token) {
		this.token = token;
	}

	@Override
	public final boolean isQualified(Map<String, Object> params, String qualifier) throws RiskEngineApplicativeException {
		return getQualifierToken(qualifier).equals(qualify(params));
	}
	
	private String getQualifierToken(String qualifier) {
		StringTokenizer st = new StringTokenizer(qualifier, "_");
		for (int i = 0; st.hasMoreTokens() && i < st.countTokens(); i++) {
			String token = st.nextToken();
			if (i == getToken())
				return token;
		}
		return null;
	}
	
	protected abstract String qualify(Map<String, Object> params) throws RiskEngineApplicativeException;

}

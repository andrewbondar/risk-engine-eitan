package com.fidocredit.riskengine.qualifier;

import java.util.Map;

public class FinancialProduct extends AbstractQualifier {

	protected FinancialProduct() {
		super(2);
	}

	@Override
	protected String qualify(Map<String, Object> params) {
		return Qualifier.FinancialProduct.ALL.getProduct();
	}

}

package com.fidocredit.riskengine;

import java.util.Map;

import com.fidocredit.riskengine.exceptions.RiskEngineApplicativeException;

public interface RiskModelScore {

	RiskModelScoreResult scoreModel(Map<String, Object> params) throws RiskEngineApplicativeException;

	Map<String, Object> enrichData(Map<String, Object> params) throws RiskEngineApplicativeException;

}

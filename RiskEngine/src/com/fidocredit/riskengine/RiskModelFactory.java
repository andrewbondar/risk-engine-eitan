package com.fidocredit.riskengine;

import java.util.Map;

import com.fidocredit.riskengine.exceptions.RiskEngineApplicativeException;

public abstract class RiskModelFactory {
	
	public abstract RiskModelScore getRiskModelScore(Map<String, Object> params) throws RiskEngineApplicativeException;
	
}

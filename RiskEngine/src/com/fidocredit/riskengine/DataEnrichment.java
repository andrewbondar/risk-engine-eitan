package com.fidocredit.riskengine;

import java.util.List;
import java.util.Map;

import com.fidocredit.riskengine.exceptions.InputParseException;
import com.fidocredit.riskengine.exceptions.RiskEngineApplicativeException;
import com.fidocredit.riskengine.lms.Account;

public interface DataEnrichment {

	/**
	 * 
	 * @param params
	 * @return No of loans who indicated this customer as their reference
	 */
	Double getReferenceScore(Map<String, Object> params) throws RiskEngineApplicativeException;

	/**
	 * 
	 * @param params
	 * @return
	 */
	Long getHowManyReferences(Map<String, Object> params) throws RiskEngineApplicativeException;

	/**
	 * 
	 * @param params
	 * @return whether this customer as never applied before, regardless of whether he indicated so or not
	 */
	boolean isNewCustomer(Map<String, Object> params) throws RiskEngineApplicativeException;

	List<Account> getAccounts(Map<String, Object> params) throws InputParseException;

}
package com.fidocredit.riskengine.lms.entity;

import java.sql.Timestamp;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-02-08T05:42:13.846+0200")
@StaticMetamodel(Survey.class)
public class Survey_ {
	public static volatile SingularAttribute<Survey, SurveyPK> id;
	public static volatile SingularAttribute<Survey, Timestamp> createdOn;
	public static volatile SingularAttribute<Survey, String> fieldValue;
	public static volatile SingularAttribute<Survey, Timestamp> modifiedOn;
}

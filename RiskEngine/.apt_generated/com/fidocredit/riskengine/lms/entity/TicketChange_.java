package com.fidocredit.riskengine.lms.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-02-08T05:42:13.862+0200")
@StaticMetamodel(TicketChange.class)
public class TicketChange_ {
	public static volatile SingularAttribute<TicketChange, TicketChangePK> id;
	public static volatile SingularAttribute<TicketChange, String> author;
	public static volatile SingularAttribute<TicketChange, String> newvalue;
	public static volatile SingularAttribute<TicketChange, String> oldvalue;
}

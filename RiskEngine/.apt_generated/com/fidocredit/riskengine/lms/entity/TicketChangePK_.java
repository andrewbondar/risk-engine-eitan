package com.fidocredit.riskengine.lms.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-02-08T05:42:13.862+0200")
@StaticMetamodel(TicketChangePK.class)
public class TicketChangePK_ {
	public static volatile SingularAttribute<TicketChangePK, Integer> ticket;
	public static volatile SingularAttribute<TicketChangePK, Long> time;
	public static volatile SingularAttribute<TicketChangePK, String> field;
}

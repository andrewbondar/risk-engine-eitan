package com.fidocredit.riskengine.lms.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="Dali", date="2016-02-08T05:42:13.862+0200")
@StaticMetamodel(Ticket.class)
public class Ticket_ {
	public static volatile SingularAttribute<Ticket, Integer> id;
	public static volatile SingularAttribute<Ticket, String> cc;
	public static volatile SingularAttribute<Ticket, Long> changetime;
	public static volatile SingularAttribute<Ticket, String> component;
	public static volatile SingularAttribute<Ticket, String> description;
	public static volatile SingularAttribute<Ticket, String> keywords;
	public static volatile SingularAttribute<Ticket, String> milestone;
	public static volatile SingularAttribute<Ticket, String> owner;
	public static volatile SingularAttribute<Ticket, String> priority;
	public static volatile SingularAttribute<Ticket, String> reporter;
	public static volatile SingularAttribute<Ticket, String> resolution;
	public static volatile SingularAttribute<Ticket, String> severity;
	public static volatile SingularAttribute<Ticket, String> status;
	public static volatile SingularAttribute<Ticket, String> summary;
	public static volatile SingularAttribute<Ticket, Long> time;
	public static volatile SingularAttribute<Ticket, String> type;
	public static volatile SingularAttribute<Ticket, String> version;
}

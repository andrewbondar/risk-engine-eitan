package com.fidocredit.riskengine.servlet;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.MDC;

@WebFilter(filterName = "SessionIdFilter",
urlPatterns = {"/*"})
public class Filter implements javax.servlet.Filter, com.sun.org.apache.xalan.internal.xsltc.dom.Filter {

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {
	    HttpServletRequest httpRequest = (HttpServletRequest) arg0;
	    HttpSession session = httpRequest.getSession();
	    if (session != null) {
	      MDC.put("sessionID", session.getId());
	      MDC.put("appName", System.getProperty("appName", "risk-engine"));
	    }
	    arg2.doFilter(arg0, arg1);
	}

	@Override
	public boolean test(int node) {
		return false;
	}

	@Override
	public void destroy() {
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}

}

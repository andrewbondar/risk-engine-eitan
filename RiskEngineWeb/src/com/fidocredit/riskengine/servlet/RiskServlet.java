package com.fidocredit.riskengine.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.fidocredit.riskengine.RiskInputParams;
import com.fidocredit.riskengine.RiskModelScoreResult;
import com.fidocredit.riskengine.ejb.RiskScoreEJBLocal;
import com.fidocredit.riskengine.exceptions.InputParseException;
import com.fidocredit.riskengine.exceptions.RiskEngineApplicativeException;

/**
 * Servlet implementation class RiskServlet
 */
@WebServlet("/riskscore")
public class RiskServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    @EJB
    private RiskScoreEJBLocal rsl;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RiskServlet() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    	doPost(req, resp);
    }
    
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String loan_id = "unknown";
		try {
			
			Map<String, Object> params = new HashMap<String, Object>();
			
			if (MediaType.APPLICATION_JSON.equals(request.getContentType())) {
				StringBuffer jb = new StringBuffer();
				String line = null;
				BufferedReader reader = request.getReader();
				while ((line = reader.readLine()) != null)
					jb.append(line);
			
				String postJsonPayload = jb.toString();
				Logger.getLogger(this.getClass()).info("json payload :" + postJsonPayload);
				JSONParser parser = new JSONParser();
				JSONObject jpl = null;
				try {
					jpl = (JSONObject) parser.parse(postJsonPayload);
					params = jpl;
				} catch (ParseException e) {
					throw new InputParseException("error parsing json payload");
				}
			} else if (MediaType.APPLICATION_FORM_URLENCODED.equals(request.getContentType()) || request.getContentType() == null) {
				Map<String, String[]> inParams = request.getParameterMap();
				Logger.getLogger(this.getClass()).info("urlencoded payload :" + inParams.toString());
			    Iterator<Entry<String, String[]>> it = inParams.entrySet().iterator();
			    while (it.hasNext()) {
			        Map.Entry<String, String[]> pair = it.next();
			        params.put(pair.getKey(), pair.getValue()[0]);
			    }
			} else {
				throw new InputParseException("unsupported content type");
			}
			
			for (Iterator<Entry<String, Object>> it = params.entrySet().iterator(); it.hasNext(); ) {
				Entry<String, Object> entry = it.next();
				Logger.getLogger(this.getClass()).info("payload entry " + entry.getKey() + " : " + entry.getValue());
				if (entry.getKey().equals(RiskInputParams.LOAN_ID.getParamName()))
					loan_id = entry.getValue().toString();
			}
			
			RiskModelScoreResult rmsr = rsl.scoreModel(params);
			JSONObject ret = new JSONObject();
			ret.put("Decision", rmsr.isAccepted());
			ret.put("ModelID", rmsr.getModel());
			ret.put("ScoreCard", rmsr.getScore());

			Logger.getLogger(this.getClass()).info("json response for loan id " + params.get(RiskInputParams.LOAN_ID.getParamName()) + " :" + ret.toJSONString());
			
			response.getWriter().append(ret.toJSONString());
		} catch (RiskEngineApplicativeException reap) {
			Logger.getLogger(this.getClass()).error("error for loan id " + loan_id + " " + reap.getClass().getSimpleName() + " : " + reap.getMessage());
			reap.printStackTrace();
			response.sendError(400, reap.getClass().getSimpleName() + " : " + reap.getMessage());
		} catch (NullPointerException npe) {
			Logger.getLogger(this.getClass()).error("error for loan id " + loan_id + " " + npe.getClass().getSimpleName() + " : " + npe.getMessage());
			npe.printStackTrace();
			response.sendError(400, npe.getClass().getSimpleName());
		} catch (NumberFormatException nfe) {
			Logger.getLogger(this.getClass()).error("error for loan id " + loan_id + " " + nfe.getClass().getSimpleName() + " : " + nfe.getMessage());
			nfe.printStackTrace();
			response.sendError(400, nfe.getClass().getSimpleName());
		} catch (EJBException ejbe) {
			ejbe.printStackTrace();
			Logger.getLogger(this.getClass()).error("error for loan id " + loan_id + " " + ejbe.getCausedByException().getClass().getSimpleName() + " : " + ejbe.getCausedByException().getMessage());
			if (ejbe.getCausedByException() instanceof NumberFormatException)
				response.sendError(400, ejbe.getCausedByException().getClass().getSimpleName());
			else
				response.sendError(500, ejbe.getCausedByException().getClass().getSimpleName());
		} catch (Throwable t) {
			Logger.getLogger(this.getClass()).error("error for loan id " + loan_id + " " + t.getClass().getSimpleName() + " : " + t.getMessage());
			t.printStackTrace();
			response.sendError(500, t.getClass().getSimpleName());
		}
	}

}

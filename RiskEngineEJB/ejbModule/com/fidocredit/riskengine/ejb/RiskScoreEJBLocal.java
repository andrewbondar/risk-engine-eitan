package com.fidocredit.riskengine.ejb;

import java.util.Map;

import javax.ejb.Local;

import com.fidocredit.riskengine.RiskModelScoreResult;
import com.fidocredit.riskengine.exceptions.RiskEngineApplicativeException;

@Local
public interface RiskScoreEJBLocal {
	
	public RiskModelScoreResult scoreModel(Map<String, Object> params) throws RiskEngineApplicativeException;

}

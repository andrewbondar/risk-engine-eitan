package com.fidocredit.riskengine.ejb;

import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.log4j.Logger;

import com.fidocredit.riskengine.RiskEngine;
import com.fidocredit.riskengine.RiskModelScoreResult;
import com.fidocredit.riskengine.exceptions.RiskEngineApplicativeException;

/**
 * Session Bean implementation class RiskScoreEJB
 */
@Stateless
public class RiskScoreEJB implements RiskScoreEJBLocal {

	@Inject RiskEngine re;
	
    /**
     * Default constructor. 
     */
    public RiskScoreEJB() {

    }

	@Override
	public RiskModelScoreResult scoreModel(Map<String, Object> params) throws RiskEngineApplicativeException {
		Logger.getLogger(this.getClass()).debug("calling RiskEngine.modelScoring from RiskScoreEJB");
		return re.modelScoring(params);
	}

}
